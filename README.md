# GoToSocial
GoToSocial docker-compose.yml

GoToSocial: [Github](https://github.com/superseriousbusiness/gotosocial) | [DockerHub](https://hub.docker.com/r/superseriousbusiness/gotosocial)

»GoToSocial is an ActivityPub social network server, written in Golang.«